#!/usr/bin/env bash

# Цветулечки
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
cyan=$(tput setaf 6)
bold=$(tput bold)
reset=$(tput sgr0)

echo "${green}Start environment initialization...${reset}"

read -p "Run docker-compose up -d? (y/n)" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "${green}Up project...${reset}"
  docker-compose up -d
fi

read -p "Run php artisan migrate? (y/n)" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
  docker exec -it tz_test_1 php artisan migrate
  docker exec -it tz_test_1 php artisan queue:flush
fi

read -p "Run php artisan events:generate? (y/n)" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "${green}Run events:generate command...${reset}"
  docker exec -it tz_test_1 php artisan events:generate
fi

echo "${green} ${reset}"
echo "${green}1) Посетите http://localhost:8080/horizon/dashboard для отслеживания процесса разбора очереди${reset}"
echo "${green}2) Файлы логов лежат в дериктории ../laravel/storage/app/test ${reset}"
echo "${green} ${reset}"

read -p "Watch \"failed\" jobs? (y/n)" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "${green}Run events:generate command...${reset}"
  docker-compose logs -f | grep "ERRRRROR"
fi


echo "${green}Run.sh successfully finished!${reset}"

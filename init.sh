#!/usr/bin/env bash

# Цветулечки
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
cyan=$(tput setaf 6)
bold=$(tput bold)
reset=$(tput sgr0)

echo "${green}Start environment initialization...${reset}"

resources=('.env')
composer_projects=('laravel')
parentdir=$(dirname $(pwd))

for fileName in "${resources[@]}"; do
  echo "$fileName"

  replaceFile=true
  if [ -f ${fileName} ]; then
    echo "${yellow}File \"$fileName\" already exist.${reset}"
    read -p "Replace it? (y/n)" -n 1 -r
    echo ""

    if [[ $REPLY =~ ^[Yy]$ ]]; then
      mv ${fileName} ${fileName}.bac
      echo "  Current \"$fileName\" moved to \"$fileName.bac\"!"
    else
      replaceFile=false
    fi
  fi

  if [ "$replaceFile" = true ]; then
    cp ${fileName}.example ${fileName}
    echo "  New \"$fileName\" created from example!"
  fi
done

read -p "Clone projects? (y/n)" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "${green}Clone projects...${reset}"

  # Clone projects
  for projectName in "${composer_projects[@]}"; do
    echo "${green}Clone https:\/\/gitlab.com/test_bothelp/$projectName.git into $projectName${reset}"
    git clone https://gitlab.com/test_bothelp/"$projectName.git" "$parentdir/$projectName"
    cp $parentdir/$projectName/.env.example $parentdir/$projectName/.env
  done
fi

read -p "Run run.sh? (y/n)" -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
  ./run.sh
fi

echo "${green}Initialization successfully finished!${reset}"

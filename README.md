# docker-tz_test
Docker Compose конфигурация с PHP 7.2

## Введение
Итак, для работы требуется установить 
* [Docker CE](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install)

# Структура папок
создаем такую стрктуру папок

| ./      |                                              |
| ------- | -------                                      |
| -> | test( git clone <test_project>) |
| -> | data                                              |
| -> | docker( git clone <this_project>)                 |

#Запуск
1) git clone <this_project>
2) ./init.sh (при первом запуске тыкаем везде "y")
3) Если есть желание переходим на страницу http://localhost:8080/horizon/dashboard и отслеживаем процесс разбора очереди. Файлы логов в папке ../laravel/storage/app/test

# Запуск вручную
1) Сперва копируем `.env.example` в `.env`
2) Потом в `.env` можем поменять пути в TEST_VOLUME, в POSTGRES_DATA и в REDIS_DATA_PATH(лучше писать абсолютные).
3) Для запуска из этой директории пишем в терминал `docker-compose up` или `docker-compose up -d`(запуск в качестве демона, т.е без вывода логов)
4) Следуем указаниям из readme в проекте 

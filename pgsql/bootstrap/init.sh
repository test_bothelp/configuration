#!/bin/bash

# $1 - Database name
createDatabase ()
{
    echo "CREATE DATABASE ${1};" | psql -U postgres

    return 0
}


# $1 - Database name
# $2 - User name
grantUser ()
{
    echo "GRANT ALL PRIVILEGES ON DATABASE ${1} TO ${2};" | psql -U postgres

    return 0
}

# $1 - Database name
# $2 - Dump name
restoreDump ()
{
    BASENAME=$(basename "${2}")

    echo
    echo "Start to restore dump \"${BASENAME}\" to database \"${1}\""

    if [[ ${2} == *.gz ]]; then
        gunzip -c ${2} | psql -q -U postgres ${1}
    else
        if [[ ${2} == *.sql ]]; then
            psql -q -U postgres ${1} < ${2}
        fi
    fi

    echo
    echo "Dump \"${BASENAME}\" was successfully restored to database \"${1}\"!"

    return 0
}

# Костыль, без которого невозможно загрузить дамп от Макса
# @TODO: Нормализовать дамп
psql -U postgres -c 'CREATE ROLE max;'
psql -U postgres -c 'ALTER ROLE max WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION;'

if [ -d $DUMPS_PATH ]; then
    for file in $DUMPS_PATH/*
    do
        if [[ -f $file ]]; then
            BASENAME=$(basename "${file}")
            DATABASE_NAME="${BASENAME%%.*}"

            createDatabase $DATABASE_NAME
            grantUser $DATABASE_NAME $POSTGRES_USER
            restoreDump $DATABASE_NAME $file
        fi
    done
fi